# nile-dev

## issues

* Pstore doesn't work, see last changes in drivers/power and defconfig (had to do that due to USB-OTG only working after cold boot)
* Incall audio on DSDS (unconfirmed)
* IMS (unconfirmed)

## manifest

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <remote fetch="https://gitlab.com" name="gitlab" />

  <project name="LineageOS/android_hardware_sony_macaddrsetup" path="hardware/sony/macaddrsetup" remote="github" />

  <project name="TheMuppets/proprietary_vendor_sony" path="vendor/sony" remote="github" />
  <project name="LineageOS/android_kernel_sony_sdm660" path="kernel/sony/sdm660" remote="github" />
  <project name="LineageOS/android_device_sony_nile-common" path="device/sony/nile-common" remote="github" />
  <project name="LineageOS/android_device_sony_discovery" path="device/sony/discovery" remote="github" />
  <project name="LineageOS/android_device_sony_pioneer" path="device/sony/pioneer" remote="github" />
</manifest>
```

## building

```
repo sync
. build/envsetup.sh
brunch {pioneer|discovery}
```